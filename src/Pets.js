import React, { Component } from 'react';
import {StyleSheet, Image, Text, View, ScrollView,TouchableHighlight,TouchableOpacity, Alert} from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {AsyncStorage} from 'react-native';
import * as firebase from 'firebase';
import '@firebase/firestore';
class Welcome extends Component {
    database = firebase.database();
    constructor(props)
    {
        super(props);
        this.state = {
            pets:[]
        }
    }

    componentDidMount()
    {
        let self = this;
        this.getuserid().then(function(userid){
            self.getpets(userid);
        })
    }

    getuserid = async() => {
        let userid = await AsyncStorage.getItem('userid');
        return userid;
    }

    getpets = (userid) => {
        let self = this;
        this.database.ref("/pet").on('value',snapshot=>{
            var pets = [];
            if(snapshot.exists)
            {
                snapshot.forEach(c=>{
                    var valuedata = c.val();
                    valuedata.key = c.key;
                    if(valuedata.userid && valuedata.userid == userid)
                    {
                        pets.push(valuedata);
                    }
                })
            }

            self.setState({
                pets:pets
            });
        })
    }

    click = (pet) => {
        console.log(this.props.navigation.state.params);
        if(this.props.navigation.state.params && this.props.navigation.state.params.addpet)
        {
            console.log("pet");
            this.props.navigation.goBack(0);
            this.props.navigation.state.params.addpets(pet);
        }
    }

    delete = (index) => {
        let petkey = this.state.pets[index].key;
        this.database.ref('/pet/' + petkey).remove();
        let pets = this.state.pets;
        pets.splice(index,1);
        this.setState({
            pets:pets
        })
    }

    petDelete = (index) => {
        Alert.alert("Delete a Registered Pet","Want to delete a pet?",[{text:"OK",onPress:()=>this.delete(index)}]);
    }

    render(){
        let self = this;
        return(
            <View style={{flex: 1}}>
                <View style={styles.backButtonContainer}>   
                    <Image style={{width: 44, height: 30, marginTop: 16, paddingRight: 10}} source={require('../img/logotipo.png')}></Image>                 
                    <Text style={styles.backToHomeText}>Pets</Text>
                </View>
            <ScrollView style={styles.container}>
                {
                    this.state.pets.length == 0 && (
                        <View style={{paddingLeft: 16, paddingRight: 16, flex: 1}}>
                            <View>
                                <Text>Nenhum animal de estimação registrado. Por favor, adicione um novo.</Text>
                            </View>
                            <View style={styles.addNewBtnContainer}>
                                <TouchableOpacity style={styles.addNewBtnStyle} onPress={()=>this.props.navigation.navigate('RegisterPet')}>
                                    <Text style={styles.addNewBtnTextStyle}>
                                        Adicionar novo
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                    )
                }
                {
                    this.state.pets.map((row,index)=>{
                        return (
                            <View key={index}>
                                <TouchableOpacity style={styles.listItemContainer1} onPress={()=>this.click(row)} onLongPress = {() => self.petDelete(index)}>
                                    <Image style={styles.petAvatar} source={{uri:row.file}}></Image>
                                    <Text style={{fontSize: 20, paddingLeft: 20, width: '78%'}}>{row.name}<Text style={{fontSize: 14, color: '#858585'}}>{'\n'}Cachoro . {row.idade} . Grande</Text></Text>
                                    <TouchableHighlight onPress={()=>this.props.navigation.navigate('RegisterPet',{data:row})}>
                                        <Image style={styles.listItemImage1} source={require('../img/arrow_forward.png')}></Image>
                                    </TouchableHighlight>
                                </TouchableOpacity>
                                <View style={styles.boarder3}></View>                       
                            </View>
                        )
                    })
                }
            </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    backButtonContainer: {
        flexDirection: 'row',
        marginTop: 56,
        paddingLeft: 16,
        paddingRight: 16,
        paddingBottom: 22,
        borderBottomColor: '#efefef',
        borderBottomWidth: 1
    },
    petAvatar: {
        width: 60,
        height: 60,
        borderRadius: 30,
        paddingLeft: 16,
        paddingRight: 16
    },
    backToHomeText: {
        fontSize: 24,
        marginTop: 16,
        marginLeft: 12,
        fontWeight: '400'
    },
    addNewBtnContainer: {
        flex: 1,
        alignItems: 'center',
        marginTop: hp('2%'),
        height: hp('6.5%')
    },
    addNewBtnStyle: {
        backgroundColor: '#FFC700',
        borderRadius: 6,
        width: '100%',
        height: hp('6.5%'),
        padding:0
    },
    addNewBtnTextStyle: {
        alignSelf: 'center',
        color: '#000',
        fontSize: 16,
        fontWeight: 'bold',
        paddingTop: 10,
        paddingBottom: 10,
    },
    boader1 :{
        backgroundColor: '#e9e9e9',
        height: 2,
        width: '100%',
        marginTop: 30
    },
    serviceTitleContainer: {
        flex: 1,
        height: 58,
        justifyContent: 'center',
        paddingLeft: 16,
        paddingRight: 16,
    },
    serviceTitle: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    serviceImgContainer1: {
        width: 126,
        marginRight: 20,
    },
    serviceText: {
        height: 48,
        justifyContent: 'center',
    }, 
    horizontalScrollView: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 16,
        paddingRight: 16
    },
    boader2 :{
        backgroundColor: '#e9e9e9',
        height: 6,
        width: '100%',
        marginTop: 31
    },
    listItemContainer1 :{
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 16,
        paddingRight: 16,
        marginTop: 8,
        height: 44
    },
    listItemText1: {
        fontSize: 14,
        width: '75%',
        marginLeft: 16,
        paddingTop: 16,
        height: 44
    },
    boarder3: {
        backgroundColor: '#e9e9e9',
        height: 2,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 23
    },
    listItemImage1: {
        marginTop: 20
    }
})

const RootStack = createStackNavigator({
    servicelist: { 
        screen: Welcome,
        navigationOptions: {
            header: null
        }
    }
});
  
const AppContainer = createAppContainer(RootStack);
export default AppContainer;
