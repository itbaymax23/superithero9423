import React, { Component } from 'react';
import {StyleSheet, Text, Image, View, ScrollView,Alert} from 'react-native';
import * as firebase from 'firebase';
import '@firebase/firestore';
import {AsyncStorage} from 'react-native';
class Agenda extends Component {
    firebase = firebase.database();
    constructor(props)
    {
        super(props);
        this.state = {
            pets:[]
        }
    }
    componentDidMount()
    {
        let self = this;
        
        this.updateinfo()
        
    }

    getpets = async() => {
        let self = this;
        let userid = await this.getuserid();
        console.log(userid);
        return new Promise((resolve,reject)=>{
            self.firebase.ref("/pet").on('value',snapshot=>{
                let data = [];
                snapshot.forEach(c=>{
                    let value = c.val();
                    if(value.userid == userid)
                    {
                        let value = c.val();
                        value.key = c.key;
                        data.push(value);
                    }
                    
                })
                resolve(data);
            })
        })
    }

    updateinfo = async() => {
        let userid = await this.getuserid();
        let pets = await this.getpets();
        console.log(pets);
        var data = [];
        let self = this;
        this.firebase.ref('/userservice').on('value',snapshot=>{
            snapshot.forEach(c=>{
                let value = c.val();
                if(value.userid == userid)
                {
                    for(let item in value.pets)
                    {
                        for(let itempets in pets)
                        {
                            if(value.pets[item] == pets[itempets].key)
                            {
                                let dataitem = c.val();
                                dataitem.key = c.key;
                                dataitem.pets = pets[itempets];
                                data.push(dataitem);
                                break;
                            }
                        }
                    }
                }
            })

            console.log(data);
            self.setState({
                pets:data
            })
        })
    }

    getuserid = async() => {
        let userid = await AsyncStorage.getItem('userid');
        return userid;
    }

    deletefunction = (data,index) =>
    {
        Alert.alert("Do you confirm to cancel this service?","",[{text:"OK",onPress:()=>this.delete(data,index)}]);
    }
    delete = (data,index) => {
        let self = this;
        
        this.firebase.ref('/userservice/' + data.key).on('value',snapshot=>{
            snapshot.forEach(c=>{
                let value = c.val();
                console.log(value);
                for(item in value.pets)
                {
                    if(value.pets[item] == data.pets.key)
                    {
                        value.pets.splice(item,1);
                        break;
                    }
                }

                if(value.pets && value.pets.length > 0)
                {
                    self.firebase.ref('/userservice/' + data.key).set(value);
                }
                else{
                    self.firebase.ref('/userservice/' + data.key).remove();
                }
            })

            let pets = self.state.pets;
            pets.splice(index,1);
            self.setState({
                pets:pets
            })
        })
    }
    render(){
        return(
            <ScrollView style={styles.container}>
                <Text style={{marginTop: 44, height: 78, paddingTop: 27, paddingLeft: 15, paddingRight: 15, fontSize: 24}}>Agenda</Text>
                <View style={styles.agendaTextContainer}>
                    <Text style={{fontWeight: 'bold', fontSize: 12, marginTop: 18, paddingLeft: 15, paddingRight: 15}}>Em caso de cancelamentos</Text>
                    <Text style={{color: '#858585', fontSize: 12, lineHeight: 14, paddingLeft: 15, marginTop: 8 }}>O cancelamento é permitido em até 24h antes do horário de{'\n'}serviço. Qualquer 
                    dúvida,<Text style={{color: '#ff4000', fontWeight: 'bold'}}> liga pra gente!</Text> </Text>
                </View>
                {
                    this.state.pets.map((row,index)=>{
                        return (
                            <View key={index}>
                                 <View style={styles.petItemContainer}>
                                    <Image source={{uri:row.pets.file}} style={{width: 70, height: 70}}></Image>
                                    <Text style={{fontSize: 24, marginLeft: 18, height: 54, paddingTop: 15}}>{row.pets.name}</Text>
                                </View>
                                <View style={styles.detailContainer}>
                                    <Text style={{fontSize: 14, fontWeight: 'bold', color: '#ff4000'}}>{row.date}{'\n'}<Text style={{color: '#000', fontSize: 14, }}>{row.service}</Text></Text>
                                    <Text style={{fontSize: 14, color: '#858585', marginLeft: 30}}>Horário:{'\n'}<Text style={{color: '#000', fontSize: 14, }}>{row.time}</Text></Text>
                                    <Text style={{color: '#ff4000', marginLeft: 120 }} onPress={()=>this.deletefunction(row,index)}>Cancelar</Text>
                                </View>
                            </View>
                        )
                    })
                }
               
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    agendaTextContainer: {
        flex: 1,
        height: 84,
        backgroundColor: '#efefef'
    },
    petItemContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 15
    },
    detailContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 19
    }
})

export default Agenda; 