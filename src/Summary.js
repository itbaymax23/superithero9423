import React, { Component } from 'react';
import {StyleSheet, Image, TextInput, ScrollView, View, Text, Switch,TouchableHighlight} from 'react-native';
import * as firebase from 'firebase';
import '@firebase/firestore';
import {AsyncStorage} from 'react-native';
class Services extends Component {
    servicedata = {};
    constructor(props) {
        super(props);
        this.state={
            switchValue: false,
            price:props.navigation.state.params.service.price * props.navigation.state.params.data.pets.length,
            tip:[
                {
                    price:55,
                    value:0
                },
                {
                    price:55,
                    value:0
                },
                {
                    price:55,
                    value:0
                }
            ],
            payment:[],
            paymentvalue:''
        }
    }

    addValue = (item) => {
        tip = this.state.tip;
        tip[item].value ++;
        this.setState({
            tip:tip
        })
    }
    getuserid = async() => 
    {
        let userid = await AsyncStorage.getItem('userid');
        return userid;
    }
    removevalue = (item) => {
        tip = this.state.tip;
        tip[item].value = Math.max(tip[item].value - 1,0);
        this.setState({
            tip:tip
        })
    }

    calcprice  = () => {
        tip = this.state.tip;
        let total = Number(this.state.price);
        console.log(tip);
        for(item in tip)
        {   
            total += tip[item].price * tip[item].value;
        }
        
        console.log(total);
        return Math.floor(total);
    }

    selectpayment = (data) => {
        let payment = this.state.payment;
        console.log(data);
        payment.push(data);
        this.setState({
            payment:payment
        })
    }

    setpayment = (value,payment) => {
        if(value)
        {
            this.setState({
                paymentvalue:payment
            })
        }
        else
        {
            this.setState({
                paymentvalue:''
            })
        }
    }

    addpets = async() => {
        console.log(this.props.navigation.state.params);
        let userid = await this.getuserid();
        data = this.props.navigation.state.params.data;
        let pets = [];
        for(item in data.pets)
        {
            pets.push(data.pets[item].key);
        }

        data.pets = pets;

        data.service = this.props.navigation.state.params.service.name;

        data.tips = this.state.tip;
        data.userid = userid;

        let database = firebase.database();
        database.ref("/userservice").push(data);
        this.props.navigation.navigate("confirmcheckout");
    }

    render() {
        let imageurl0 = this.state.tip[0].value > 0?'../img/remove_item.png':'../img/remove_inactive.png';
        let imageurl1 = this.state.tip[1].value > 0?'../img/remove_item.png':'../img/remove_inactive.png';
        let imageurl2 = this.state.tip[2].value > 0?'../img/remove_item.png':'../img/remove_inactive.png';
        return(
            <ScrollView style = {styles.container}>
                <View style={styles.backButtonContainer}>
                    <TouchableHighlight onPress={()=>this.props.navigation.goBack()}>
                        <Image style={styles.backButton} source={require('../img/arrow_back.png')}></Image>
                    </TouchableHighlight>
                    <Text style={styles.backToHomeText}>Resumo</Text>
                 </View>
                 <View style={{paddingLeft: 15, paddingRight: 15, height: 48}}>
                     <Text style={{fontWeight: 'bold', lineHeight: 14, fontSize: 12, paddingTop: 11}}>{this.props.navigation.state.params.service.name} - Cachorro ({this.props.navigation.state.params.data.pets[0].name}){'\n'}Porte Grande</Text>
                 </View>
                 <View style={styles.detailsContainer}>
                    <Text style={{flex: 1, height: 48}}>Dia:{'\n'}{this.props.navigation.state.params.data.date}</Text>
                    <Text style={{flex: 1, height: 48}}>Horário:{'\n'}{this.props.navigation.state.params.data.time}</Text>
                    <Text style={{flex: 1, height: 48}}>Valor:{'\n'}R$ {this.props.navigation.state.params.service.price}</Text>
                 </View>
                 <View style={styles.buyTextContainer}>
                     <Text style={styles.buyText}>Compre junto!</Text>
                 </View>
                <View style={styles.dogCollarContainer}>
                    <View style={styles.dogCollarItemContainer1}>
                        <Image source={require('../img/dog_collar1.png')}></Image>
                        <Text style={{fontSize: 11}}>Coleira para{'\n'}cachorros Zee.Dog{'\n'}Toy Story - Mr Pot...</Text>
                        <Text style={{fontWeight: 'bold', height: 30, paddingTop: 9}}>R$ 55,00</Text>
                        <View style={styles.actionicon}>
                            <TouchableHighlight onPress={() => this.removevalue(0)}>
                                <Image source={require('../img/remove_item.png')} style={{width:16,height:16}}></Image>
                            </TouchableHighlight>
                            
                            <Text style={styles.icons}>{this.state.tip[0].value}</Text>
                            <TouchableHighlight onPress={() => this.addValue(0)}>
                                <Image source={require('../img/add_item.png')} style={{width:16,height:16}}></Image>
                            </TouchableHighlight>
                            
                        </View>
                    </View>
                    <View style={styles.dogCollarItemContainer1}>
                        <Image source={require('../img/dog_collar1.png')}></Image>
                        <Text style={{fontSize: 11}}>Coleira para{'\n'}cachorros Zee.Dog{'\n'}Toy Story - Mr Pot...</Text>
                        <Text style={{fontWeight: 'bold', height: 30, paddingTop: 9}}>R$ 55,00</Text>
                        <View style={styles.actionicon}>
                            
                            <TouchableHighlight onPress={() => this.removevalue(1)}>
                                <Image source={require('../img/remove_item.png')} style={{width:16,height:16}}></Image>
                            </TouchableHighlight>
                            <Text style={styles.icons}>{this.state.tip[1].value}</Text>
                            <TouchableHighlight onPress={() => this.addValue(1)}>
                                <Image source={require('../img/add_item.png')} style={{width:16,height:16}}></Image>
                            </TouchableHighlight>
                        </View>
                    </View>
                    <View style={styles.dogCollarItemContainer1}>
                        <Image source={require('../img/dog_collar1.png')}></Image>
                        <Text style={{fontSize: 11}}>Coleira para{'\n'}cachorros Zee.Dog{'\n'}Toy Story - Mr Pot...</Text>
                        <Text style={{fontWeight: 'bold', height: 30, paddingTop: 9}}>R$ 55,00</Text>
                        <View style={styles.actionicon}>
                            <TouchableHighlight onPress={() => this.removevalue(2)}>
                                <Image source={require('../img/remove_item.png')} style={{width:16,height:16}}></Image>
                            </TouchableHighlight>
                            <Text style={styles.icons}>{this.state.tip[2].value}</Text>
                            <TouchableHighlight onPress={() => this.addValue(2)}>
                                <Image source={require('../img/add_item.png')} style={{width:16,height:16}}></Image>
                            </TouchableHighlight>
                        </View>
                    </View>
                </View>

                 <View style={styles.totalContainer}>
                    <Text style={styles.totalPriceText}>Total: <Text style={{fontWeight:'bold'}}>R$ {this.calcprice()}</Text></Text>
                 </View>
                 <View style={{flex:1, paddingLeft: 15, paddingRight: 15, height: 48, marginTop: 18}}>
                     <Text style={{fontSize: 14, fontWeight: 'bold', paddingTop: 17}}>Pagamento</Text>
                 </View>
                 <View style={styles.listItemContainer1}>
                    <Text style={styles.listItemText1}>Escolher forma de pagamento</Text>
                    <TouchableHighlight onPress={() => this.props.navigation.navigate("selectpayment",{selectpayment:this.selectpayment})}>
                        <Image style={{marginTop: 14}} source={require('../img/arrow_forward.png')}></Image>
                    </TouchableHighlight>
                </View>
                <View style={{flex:1, paddingLeft: 15, paddingRight: 15, height: 48, marginTop: 30}}>
                     <Text style={{fontSize: 14, fontWeight: 'bold', paddingTop: 17}}>CPF/CNPJ na nota</Text>
                 </View>
                 <View>
                    {
                        this.state.payment.map((row,index)=>{
                            return (
                                <View key={index} style={{flex:1,flexDirection:"row", paddingLeft: 15, paddingRight: 15, height: 48, marginTop: 30}}>
                                    <Text style={{fontSize: 14, fontWeight: 'bold', paddingTop: 17}}>{row}</Text>
                                    <Switch style={{marginLeft:"auto"}} onValueChange={(value)=>{this.setpayment(value,row)}}></Switch>
                                </View>    
                            )
                            
                        })
                    }
                 </View>
                <View style={styles.confirmButtonContainer}>
                    <Text style={this.state.paymentvalue?styles.confirmEnableButton:styles.confirmButton} onPress={this.addpets}>Efetuar pagamento</Text>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backButtonContainer: {
        height: 60,
        flexDirection: 'row',
        marginTop: 44,
        paddingLeft: 18,
    },
    backButton: {
        width: 16,
        height: 16,
        marginLeft: 3,
        marginRight: 28,
        marginTop: 22
    },
    backToHomeText: {
        fontSize: 16,
        marginTop: 22
    },
    detailsContainer: {
        flex:1,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15
    },
    totalContainer: {
        flex: 1,
        width: '100%',
        height: 60,
        borderTopColor: '#cecece',
        borderTopWidth: 1,
        borderBottomColor: '#cecece',
        borderBottomWidth: 1,
        backgroundColor: '#efefef',
        marginTop: 12
    },
    totalPriceText: {
        fontSize: 18,
        marginLeft: 15,
        marginTop: 16
    },
    listItemContainer1 :{
        flex: 1,
        flexDirection: 'row',
        height: 60,
        paddingLeft: 16,
        paddingRight: 16,
        marginTop: 18,
        borderBottomColor: '#dcdcdc',
        borderBottomWidth: 1
    },
    listItemText1: {
        paddingTop: 14,
        fontSize: 16,
        width: '95%'
    },
    buyTextContainer: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        height: 48
    },
    buyText: {
        fontSize: 14,
        paddingTop: 17, 
        fontWeight: 'bold'
    },
    dogCollarContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15
    },
    dogCollarItemContainer1: {
        flex: 1,

    },
    confirmButtonContainer: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 12,
        marginBottom: 18
    },
    confirmButton: {
        textAlign: 'center',
        paddingTop: 12,
        justifyContent: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        height: 48,
        width:'100%',
        color: '#000',
        borderRadius: 6,
        backgroundColor: '#ffc700',
    },
    confirmEnableButton: {
        textAlign: 'center',
        paddingTop: 12,
        justifyContent: 'center',
        fontWeight: 'bold',
        fontSize: 18,
        height: 48,
        width:'100%',
        color: '#000',
        borderRadius: 6,
        backgroundColor: '#FFC700',
    },
    actionicon:{
        flex:1,
        flexDirection:"row",
        marginTop:10
    },
    icons:{
        width:"40%",
        textAlign:"center"
    }
})

export default Services;