import React, { Component } from 'react';
import {StyleSheet, Image, TextInput, ScrollView, View, Text, Switch,KeyboardAvoidingView ,TouchableHighlight} from 'react-native';
import * as firebase from 'firebase';
import '@firebase/firestore';
import {AsyncStorage} from 'react-native';
class RegisterAddress extends Component {
    constructor(props)
    {
        super(props);
        this.state = {
            data:{
                cep:"",
                end:"",
                num:"",
                comp:"",
                bair:"",
                cidado:"",
                estado:"",
                descendereco:""
            },
            enabled:false
        }

        //set state and city to be fixed for now
        setTimeout(() => {this.handleChange('estado', "SP")}, 500);
        setTimeout(() => {this.handleChange('cidado', "SÃO PAULO")}, 500);
    }


    handleChange = (name,value) => {
        let data = this.state.data;
        data[name] = value;

        var enable = true;

        for(item in data)
        {
            if(!data[item])
            {
                enable = false;
            }
        }
        
        console.log(enable);
        this.setState({
            data:data,
            enabled:enable
        })
    }
    getuserid = async() => 
    {
        let userid = await AsyncStorage.getItem('userid');
        return userid;
    }
    confirm = async() =>
    {
        console.log(this.state.enabled);
        if(this.state.enabled)
        {
            var data = this.state.data;
            data['userid'] = await this.getuserid();
            const db = firebase.database();
            var self = this;
            db.ref("address").push(data);

            this.props.navigation.goBack();
            //this.props.navigation.state.params.location(data);
        }
    }

    // changecomponent = (value) => {
    //     this.setState({
    //         complement:value
    //     })
    // }
    
    render() {

        let buttonstyle = {
            textAlign: 'center',
            fontSize: 18,
            color: '#DCDCDC',
            paddingTop: 10,
            height: 48,
            width: '100%',
            borderRadius: 6,
            marginTop: 36
        };
        if(this.state.enabled)
        {
           buttonstyle.backgroundColor = "#FFC700"; 
           buttonstyle.color = 'black';
        }
        
       
        return(
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <ScrollView style = {styles.container}>
                    <View style={styles.backButtonContainer}>
                        <TouchableHighlight onPress={()=>this.props.navigation.goBack(0)}>
                            <Image style={styles.backButton} source={require('../img/arrow_back.png')}></Image>
                        </TouchableHighlight>
                        <Text style={styles.backToHomeText}>Cadastro de endereço</Text>
                    </View>

                    <View style={styles.textInputContainer}>
                        <TextInput style = {styles.CPF} keyboardType="numeric" placeholder="digite seu CEP" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("cep",value)}}></TextInput>
                    </View>
                    <View style={styles.textInputContainer1}>
                        <TextInput style = {styles.CPF} placeholder="endereço" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("end",value)}}></TextInput>
                    </View>
                    {/* <View style={styles.switchContainer}>
                        <Switch value={this.state.complement} onValueChange={(value)=>{this.changecomponent(value)}}></Switch>
                        <Text style={{fontSize: 16, marginLeft: 33, paddingTop: 14}}>Sem número</Text>
                    </View> */}
                    <View style={styles.textInputContainer1}>
                        <TextInput style = {styles.CPF} keyboardType="numeric" placeholder="número" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("num",value)}} editable ={!this.state.complement}></TextInput>
                    </View>
                    <View style={styles.textInputContainer1}>
                        <TextInput style = {styles.CPF} placeholder="complemento" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("comp",value)}}></TextInput>
                    </View>
                    <View style={styles.textInputContainer1}>
                        <TextInput style = {styles.CPF} placeholder="bairro" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("bair",value)}}></TextInput>
                    </View>
                    <View style={styles.textInputContainer1}>
                        <TextInput style = {styles.CPF} editable={false} value="SÃO PAULO" placeholder="Cidade" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("cidado",value)}}></TextInput>
                    </View>
                    <View style={styles.textInputContainer1}>
                        <TextInput style = {styles.CPF} editable={false} value="SP" placeholder="Estado" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("estado",value)}}></TextInput>
                    </View>

                    <View style={styles.textInputContainer}>
                        <TextInput style = {styles.CPF} placeholder="escreva 'casa'🏘 ou 'trabalho'🏢" placeholderTextColor="#858585" onChangeText={(value)=>{this.handleChange("descendereco",value)}}></TextInput>
                    </View>

                    <View style={styles.registerButtonContainer}>
                        <Text style={buttonstyle} onPress={this.confirm}>Cadastrar endereço</Text>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    backButtonContainer: {
        height: 60,
        flexDirection: 'row',
        marginTop: 66,
        paddingLeft: 18,
    },
    backButton: {
        width: 16,
        height: 16,
        marginLeft: 3,
        marginRight: 28,
        marginTop: 16
    },
    backToHomeText: {
        fontSize: 16,
        marginTop: 16
    },
    textInputContainer: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15
    },
    CPF: {
        height: 48,
        borderBottomWidth: 1,
        borderBottomColor: '#858585',
        fontSize: 16
    },
    textInputContainer1: {
        marginTop: 18,
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15
    },
    textInputContainer2: {
        marginTop: 24,
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 15,
        paddingRight: 15
    },
    infoImage: {
        paddingTop: 16,
    },
    petOne: {
        width: '80%',
        height: 48,
        borderBottomWidth: 1,
        borderBottomColor: '#858585',
        marginRight: 37,
        fontSize: 16
    },
    registerButtonContainer: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 10,
        alignItems: 'center',
        justifyContent:'center',
        marginBottom: 20
    },
    registerButtonText: {
        textAlign: 'center',
        fontSize: 18,
        color: '#DCDCDC',
        paddingTop: 10,
        height: 48,
        width: '100%',
        borderRadius: 6,
        marginTop: 36
    },
    switchContainer: {
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 15, 
        paddingRight: 15,
        height: 48,
        marginTop: 18
    }

})

export default RegisterAddress;