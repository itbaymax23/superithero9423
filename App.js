import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import * as firebase from 'firebase';
//import Splash from './src/Splash';
//import AppContainer from './src/OnBoarding';
import SignUp from './src/SignUp';
import Welcome from './src/Welcome';
import RegisterPet from './src/RegisterPet';
import Services from './src/Services';
import ConfirmServices from './src/ConfirmServices';
import ServiceList from './src/ServiceList';
import RegistrationTip from './src/RegistrationTip';
import Summary from './src/Summary';
import Agenda from './src/Agenda';
import Checkout from './src/Checkout';
import ChooseHowToPay from './src/ChooseHowToPay';
import RegisterAddress from './src/RegisterAddress';
import CheckOutSuccess from './src/CheckOutSuccess';
import CheckOutFail from './src/CheckOutFail';
import ServiceScheduled from './src/ServiceScheduled';
import Live from './src/Live';
import StatusBar from './src/StatusBar';
import AppContainer from './src/Splash';
export default function App() {
  
   //initialize Firebase
   const firebaseConfig = {
    apiKey: 'AIzaSyAfj3GGZf484WRo8bjbWgN4NjCrg8sx7BE',
    authDomain: "petshop-e6f3b.firebaseapp.com",
    databaseURL:"petshop-e6f3b.firebaseio.com/",
    storageBucket: "petshop-e6f3b.appspot.com",
  }
  
  firebase.initializeApp(firebaseConfig);

  return (
    <View style={styles.container}>
      <AppContainer />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
